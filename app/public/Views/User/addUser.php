<!DOCTYPE html>
<html>

<head>
    <title>Editing MySQL Data</title>
</head>

<body>


    <form action="?url=save-add" method="POST" class="form" enctype="multipart/form-data">

        <h2>Add User</h2>
        <label>Username: <input type="text" value="<?php echo $row['username']; ?>" name="username"></label><br />
        <label>Fullname: <input type="text" value="<?php echo $row['fullname']; ?>" name="fullname"></label><br />
        <label>Password: <input type="text" value="<?php echo $row['password']; ?>" name="password"></label><br />
        <label>Avatar: <input type="file" value="<?php echo $row['avatar']; ?>" name="avatar"></label><br />
        <select name="department">
            <?php
            foreach ($listDepart as $depart) { ?>
                <option value="<?php echo $depart['id'] ?>"><?php echo $depart['name'] ?></option>
            <?php
            } ?>
        </select>

        <input type="submit" value="Add" name="add_user">

    </form>
</body>

</html>